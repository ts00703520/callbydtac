/**
 * @file CaasHttp.h
 */
#ifndef _CAAS_HTTP_H
#define _CAAS_HTTP_H

#import <Foundation/Foundation.h>

#import "CaasMsg.h"

/**
 * <b>Description:</b> It is used to notify the tcp is connected.
 * <br><b>Purpose:</b> After the UI invokes the sendMsg method, UI will receive this notification when tcp is connected.
 * <p>The notification will have the following extra parameters:
 * <ul>
 * <li><em>{@link #PARAM_HTTP_COOKIE}</em></li>- Indicates HTTP cookie, [unsigned int]
 * <li><em>{@link #PARAM_HTTP_MESSAGE}</em></li>- Indicates HTTP HTTP message return by the server, [CaasMsg *]
 * </ul>
 * </p>
 */
#define EVENT_HTTP_CONNECT        @"EVENT_HTTP_CONNECT"

/**
 * <b>Description:</b> It is used to broadcast a result of HTTP message.
 * <br><b>Purpose:</b> After the UI invokes the sendMsg method, UI will receives the notification, UI can get the information from the server.
 * <p>The notification will have the following extra parameters:
 * <ul>
 * <li><em>{@link #PARAM_HTTP_COOKIE}</em></li>- Indicates HTTP cookie, [unsigned int]
 * <li><em>{@link #PARAM_HTTP_MESSAGE}</em></li>- Indicates HTTP HTTP message return by the server, [CaasMsg *]
 * </ul>
 * </p>
 */
#define EVENT_HTTP_MESSAGE        @"EVENT_HTTP_MESSAGE"

/**
 * Indicates the HTTP ID in the notification NSDictionary.
 */
#define PARAM_HTTP_COOKIE         @"PARAM_HTTP_COOKIE"

/**
 * Indicates the HTTP message return by the server in the notification NSDictionary.
 */
#define PARAM_HTTP_MESSAGE        @"PARAM_HTTP_MESSAGE"

/**
 * Indicates no header need for response.
 */
#define HTTP_NEED_RSPHDR_NULL           (0)

/**
 * Indicates need {@link #EN_HTTP_IE_ETAG} for response.
 */
#define HTTP_NEED_RSPHDR_ETAG           (1 << 0)

/**
 * Indicates need {@link #EN_HTTP_IE_DATE} for response.
 */
#define HTTP_NEED_RSPHDR_DATE           (1 << 1)

/**
 * Indicates the HTTP Authentication Type.
 */
typedef enum tag_HTTP_AUTHEN_TYPE
{
    EN_HTTP_AUTHEN_WWW,        /**< Indicates Uniquely identifies www authentication   */
    EN_HTTP_AUTHEN_PROXY,      /**< Indicates Uniquely identifies proxy authentication */
    EN_HTTP_AUTHEN_NONE        /**< Indicates Uniquely identifies none authentication  */
} EN_HTTP_AUTHEN_TYPE;

/**
 * Indicates the HTTP METHOD Type.
 */
typedef enum tag_HTTP_METHOD
{
    EN_HTTP_METHOD_OPTIONS = 0,  /**< Indicates the request method is OPTIONS   */
    EN_HTTP_METHOD_GET     = 1,  /**< Indicates the request method is GET   */
    EN_HTTP_METHOD_HEAD    = 2,  /**< Indicates the request method is HEAD   */
    EN_HTTP_METHOD_POST    = 3,  /**< Indicates the request method is POST   */
    EN_HTTP_METHOD_PUT     = 4,  /**< Indicates the request method is PUT   */
    EN_HTTP_METHOD_DELETE  = 5,  /**< Indicates the request method is DELETE   */
    EN_HTTP_METHOD_TRACE   = 6,  /**< Indicates the request method is TRACE   */
    EN_HTTP_METHOD_CONNECT = 7,  /**< Indicates the request method is CONNECT   */
    
    EN_HTTP_METHOD_BUTT          /**< Indicates the butt */
} EN_HTTP_METHOD;

/**
 * Indicates the HTTP MESSAGE Type.
 */
typedef enum tag_HTTP_MSG_TYPE
{
    EN_HTTP_MSG_REQ      = 0,  /**< Indicates the request type witch using the {@link #CAAS_MSG} to send HTTP message */
    EN_HTTP_MSG_NEXT_DATA = 1,  /**< Indicates the request type witch using the {@link #CAAS_MSG} to send next HTTP message */
    EN_HTTP_MSG_RSP      = 2,  /**< Indicates the request type witch using the {@link #CAAS_MSG} to send next HTTP message */
    EN_HTTP_MSG_CONNECT  = 3,  /**< Indicates the tcp is connected */ 
    EN_HTTP_MSG_BUTT               /**< Indicates the butt */
} EN_HTTP_MSG_TYPE;

/**
 * Indicates the HTTP transfer type.
 */
typedef enum tag_HTTP_TYPE
{
    EN_HTTP_HTTP        = 0,  /* Indicates the http */
    EN_HTTP_HTTPS       = 1,  /* Indicates the https */
    EN_HTTP_BUTT              /* Indicates the butt */
} EN_HTTP_TYPE;

typedef enum tag_HTTP_CFG_TYPE 
{
    EN_HTTP_CFG_TLS_CIPHERS  =   2,  /**< Indicates the tls ciphers. */
    EN_HTTP_CFG_MAX                  /**< Indicates the butt. */
} EN_HTTP_CFG_TYPE;

/** 
 * Indicates the Date type.
 */
typedef enum tag_HTTP_DATE_TYPE
{
    EN_HTTP_DATE_LOCAL        = 0,  /* Indicates fill with local time */
    EN_HTTP_DATE_UTC          = 1,  /* Indicates fill with UTC time */
    EN_HTTP_DATE_BUTT               /* Indicates don't fill time */
} EN_HTTP_DATE_TYPE;

/**
 * Indicates the HTTP KEY Type.
 */
typedef enum tag_HTTP_IE_TYPE
{
    EN_HTTP_IE_METHOD             =   0,  /**< Indicates Indicates the HTTP message method, [CAAS_UINT]*/
    EN_HTTP_IE_AUTHNAME           =   1,  /**< Indicates the Authentication name, [CAAS_CHAR *]*/
    EN_HTTP_IE_AUTHPASSWORD       =   2,  /**< Indicates the Authentication password, [CAAS_CHAR *] */
    EN_HTTP_IE_REQUESTURI         =   3,  /**< Indicates the request URI, for example http://127.0.0.1:80/XXXX/XXXXXX*/
    EN_HTTP_IE_SERVERHOST         =   4,  /**< Indicates the remote server host, for example, the server host can be 127.0.0.1 or example.com, it can be controlled by {@link #EN_HTTP_IE_ADD_SERVERHOST}, [CAAS_CHAR *] */
    EN_HTTP_IE_SERVERADDR         =   5,  /**< Indicates the remote server address, for example it can be 127.0.0.1, [CAAS_CHAR *] */
    EN_HTTP_IE_HTTPTYPE           =   6,  /**< Indicates the HTTP transfer type, the value of it should be {@link #enum_CAAS_HTTP_TYPE}, [CAAS_UINT]*/
    EN_HTTP_IE_AUTHTYPE           =   7,  /**< Indicates the HTTP message Authentication type, the value of it should be {@link #EN_CAAS_HTTP_AUTHEN_TYPE}, [CAAS_UINT] */
    EN_HTTP_IE_AUTHWITHBODY       =   8,  /**< Indicates the Authentication with body in the HTTP message, [int]*/
    EN_HTTP_IE_CONTENTTYPE        =   9,  /**< Indicates the HTTP message content type,  {@link #EN_CAAS_HTTP_MTYPE}, [CAAS_UINT]*/
    EN_HTTP_IE_SUBCONTENTTYPE     =  10,  /**< Indicates the the HTTP message subsidiary content type,  {@link #EN_CAAS_HTTP_MSUBTYPE}, [CAAS_UINT]*/
    EN_HTTP_IE_CONTENTBODY        =  11,  /**< Indicates the HTTP message content body,  [NSData]*/
    EN_HTTP_IE_STATCODE           =  12,  /**< Indicates the state code return by the server in the HTTP message, for example it can be 200 or 403 and so on,[CAAS_UINT] */
    EN_HTTP_IE_USERAGENT          =  13,  /**< Indicates the user-agent head, [CAAS_CHAR *] */
    EN_HTTP_IE_ACCESSTOKEN        =  14,  /**< Indicates the the HTTP message with access token, [CAAS_CHAR *] */
    EN_HTTP_IE_ACCESSMSISDN       =  15,  /**< Indicates the HTTP message with access msisdn, [CAAS_CHAR *] */
    EN_HTTP_IE_NONCE              =  16,  /**< Indicates the Authentication name */
    EN_HTTP_IE_PORT               =  17,  /**< Indicates the remote server port, for example it can be 443, [CAAS_UINT] */
    EN_HTTP_IE_SERVER_DOMAIN      =  18,  /**< Indicates the server domian name, for example example.com, [CAAS_CHAR *] */
    EN_HTTP_IE_HEAD_EXTEND        =  19,  /**< Indicates the head extern configure, [CAAS_UINT] */
    EN_HTTP_IE_AUTHBODY           =  20,  /**< Indicates the Authentication body,  [CAAS_CHAR *]*/
    EN_HTTP_IE_ADD_DATE           =  22,  /**< Indicates the date need or not in the HTTP message, default is {@link #EN_HTTP_DATE_BUTT}, [CAAS_UINT] */
    EN_HTTP_IE_RSPHDR_MASK        =  23,  /**< Indicates which header need of response, {@link #HTTP_NEED_RSPHDR_NULL}, [CAAS_UINT] */
    EN_HTTP_IE_ETAG               =  24,  /**< Indicates the etag, [CAAS_CHAR *] */
    EN_HTTP_IE_DATE               =  25,  /**< Indicates the date, [CAAS_CHAR *] */
    EN_HTTP_IE_IE_CONTENT_LEN     =  26,  /**< Indicates the content length of body, [CAAS_UINT] */
    EN_HTTP_IE_IE_ONLY_SENDBODY   =  27,  /**< Indicates only send body, 0: invalid, others: valid, default is 0, it is valid when message type is {@link #EN_HTTP_MSG_NEXT_REQ}, [CAAS_UINT] */
    EN_HTTP_IE_ADD_SERVERHOST     =  28,  /**< Indicates the host need or not in the HTTP message, 0: not need, others: need, default is 1, [CAAS_UINT] */
    EN_HTTP_IE_ADD_ACCEPT         =  29,  /**< Indicates the accept need or not in the HTTP message, 0: not need, others: need, default is 1, [CAAS_UINT] */
    EN_HTTP_IE_ADD_CONNECTION     =  30,  /**< Indicates the connection need or not in the HTTP message, 0: not need, others: need, default is 1, [CAAS_UINT] */
    EN_HTTP_IE_ADD_USER_AGENT     =  31,  /**< Indicates the User-Agent need or not in the HTTP message, 0: not need, others: need, default is 1, [CAAS_UINT] */
    EN_HTTP_IE_SUPPORT_204_RSP    =  32,  /**< Indicates SDK whether resends the request without Authorization or not when receive the response 204 from server, 0: not support, others: support, default is 1, [CAAS_UINT] */
    EN_HTTP_IE_TMR_WAIT_CONN_LEN  =  37,  /**< Indicates the timer length of wait connect, default is 90s [CAAS_UINT], the unit of the timer length is second */
    
    EN_HTTP_IE_EXTHDR             =  118, /**< Indicates the extend head,  [CAAS_CHAR *] */
    EN_HTTP_IE_HDR_SET_COOKIE     =  121, /**< Indicates the "Set-Cookie" in the header, [CAAS_CHAR *] */
    
    EN_HTTP_IE_BUTT                       /**< Indicates the butt */
} EN_HTTP_IE_TYPE;

/**
 * Indicates the HTTP content Type.
 */
typedef enum tag_HTTP_MTYPE
{
    EN_HTTP_MTYPE_DISC_TEXT,     /**< Indicates the content type is text, for example Content-Type: text/ */
    EN_HTTP_MTYPE_DISC_IMAGE,    /**< Indicates the content type is image, for example Content-Type: image/ */
    EN_HTTP_MTYPE_DISC_AUDIO,    /**< Indicates the content type is audio, for example Content-Type: audio/ */
    EN_HTTP_MTYPE_DISC_VIDEO,    /**< Indicates the content type is video for example Content-Type: video/ */
    EN_HTTP_MTYPE_DISC_APP,      /**< Indicates the content type is application for example Content-Type: application/ */
    EN_HTTP_MTYPE_COMP_MSG,      /**< Indicates the content type is message for example Content-Type: message/ */
    EN_HTTP_MTYPE_COMP_MULTI,    /**< Indicates the content type is multipart for example Content-Type: multipart/*/
    EN_HTTP_MTYPE_EXT            /**< Indicates the extern type, invalue type */
} EN_HTTP_MTYPE;

/**
 * Indicates the HTTP subsidiary content Type.
 */
typedef enum tag_HTTP_MSUBTYPE
{
    EN_HTTP_MSUBTYPE_PLAIN,                     /**< Indicates the subsidiary content is plain for example: Content-Type: message/plain */
    EN_HTTP_MSUBTYPE_HTTP,                      /**< Indicates the subsidiary content is HTTP for example: Content-Type: message/HTTP */
    EN_HTTP_MSUBTYPE_OCTET_STRM,                /**< Indicates the subsidiary content is octet-stream for example: Content-Type: message/octet-stream */
    EN_HTTP_MSUBTYPE_XML,                       /**< Indicates the subsidiary content is XML for example: Content-Type: message/XML */
    EN_HTTP_MSUBTYPE_AUTH_POLICY_XML,           /**< Indicates the subsidiary content is auth-policy+xml for example: Content-Type: message/auth-policy+xml */
    EN_HTTP_MSUBTYPE_XCAP_EL_XML,               /**< Indicates the subsidiary content is xcap-el+xml for example: Content-Type: message/xcap-el+xml*/
    EN_HTTP_MSUBTYPE_XCAP_ATT_XML,              /**< Indicates the subsidiary content is xcap-att+xml for example: Content-Type: message/xcap-att+xml */
    EN_HTTP_MSUBTYPE_XCAP_NS_XML,               /**< Indicates the subsidiary content is xcap-ns+xml for example: Content-Type: message/xcap-ns+xml */
    EN_HTTP_MSUBTYPE_XCAP_ERR_XML,              /**< Indicates the subsidiary content is xcap-error+xml for example: Content-Type: message/xcap-error+xml */
    EN_HTTP_MSUBTYPE_XCAP_CAPS_XML,             /**< Indicates the subsidiary content is xcap-caps+xml for example: Content-Type: message/xcap-caps+xml */
    EN_HTTP_MSUBTYPE_XCAP_DIFF_XML,             /**< Indicates the subsidiary content is xcap-diff+xml for example: Content-Type: message/xcap-diff+xml */
    EN_HTTP_MSUBTYPE_RES_LSTS_XML,              /**< Indicates the subsidiary content is resource-lists+xml for example: Content-Type: message/resource-lists+xml */
    EN_HTTP_MSUBTYPE_RLS_SRV_XML,               /**< Indicates the subsidiary content is rls-services+xml for example: Content-Type: message/rls-services+xml */
    EN_HTTP_MSUBTYPE_OMA_XCAP_DIR_XML,          /**< Indicates the subsidiary content is xcap-directory+xml for example: Content-Type: message/xcap-directory+xml */
    EN_HTTP_MSUBTYPE_OMA_DIR_XML,               /**< Indicates the subsidiary content is oma-directory+xml for example: Content-Type: message/oma-directory+xml */
    EN_HTTP_MSUBTYPE_OMA_VND_XCAP_DIR_XML,      /**< Indicates the subsidiary content is vnd.oma.xcap-directory+xml for example: Content-Type: message/vnd.oma.xcap-directory+xml */
    EN_HTTP_MSUBTYPE_OMA_VND_GRP_USG_LST_XML,   /**< Indicates the subsidiary content is vnd.oma.group-usage-list+xml for example: Content-Type: message/vnd.oma.group-usage-list+xml */
    EN_HTTP_MSUBTYPE_OMA_VND_POC_GRPS_XML,      /**< Indicates the subsidiary content is vnd.oma.poc.groups+xml for example: Content-Type: message/vnd.oma.poc.groups+xml */
    EN_HTTP_MSUBTYPE_OMA_VND_PRES_CTT_XML,      /**< Indicates the subsidiary content is vnd.oma.pres-content+xml for example: Content-Type: message/vnd.oma.pres-content+xml */
    EN_HTTP_MSUBTYPE_OMA_VND_SEARCH_XML,        /**< Indicates the subsidiary content is vnd.oma.search+xml for example: Content-Type: message/vnd.oma.search+xml */
    EN_HTTP_MSUBTYPE_PIDF_XML,                  /**< Indicates the subsidiary content is pidf+xml for example: Content-Type: message/pidf+xml */
    EN_HTTP_MSUBTYPE_SOAP_XML,                  /**< Indicates the subsidiary content is soap+xml for example: Content-Type: message/soap+xml */
    EN_HTTP_MSUBTYPE_SYNCML_XML,                /**< Indicates the subsidiary content is vnd.syncml+xml for example: Content-Type: message/vnd.syncml+xml */
    EN_HTTP_MSUBTYPE_DM_XML,                    /**< Indicates the subsidiary content is vnd.syncml.dm+xml for example: Content-Type: message/vnd.syncml.dm+xml */
    EN_HTTP_MSUBTYPE_GZIP,                      /**< Indicates the subsidiary content is gzip for example: Content-Type: message/gzip */
    EN_HTTP_MSUBTYPE_HTTP_FT_XML,               /**< Indicates the subsidiary content is vnd.gsma.rcs-ft-http+xml for example: Content-Type: message/vnd.gsma.rcs-ft-http+xml */
    EN_HTTP_MSUBTYPE_XCAP_UT_XML,               /**< Indicates the subsidiary content is vnd.etsi.simservs+xml for example: Content-Type: message/vnd.etsi.simservs+xml */
    EN_HTTP_MSUBTYPE_X_WWW_FORM_URLENCODED_XML, /**< Indicates the subsidiary content is x-www-form-urlencoded for example: Content-Type: message/x-www-form-urlencoded */
    EN_HTTP_MSUBTYPE_FORM_DATA,                 /**< Indicates the subsidiary content is form-data for example: Content-Type: message/form-data */
    EN_HTTP_MSUBTYPE_XCAP_PUBGRP_XML,           /**< Indicates the subsidiary content is public-group+xml for example: Content-Type: message/public-group+xml */
    EN_HTTP_MSUBTYPE_XCAP_GRP_SEARCH_XML,       /**< Indicates the subsidiary content is group-search+xml for example: Content-Type: message/group-search+xml */
    EN_HTTP_MSUBTYPE_XCAP_PRV_EAB_XML,          /**< Indicates the subsidiary content is ims-pim+xml for example: Content-Type: message/ims-pim+xml */
    EN_HTTP_MSUBTYPE_IMAGE_JPEG,                /**< Indicates the subsidiary content is jpeg for example: Content-Type: image/jpeg */
    EN_HTTP_MSUBTYPE_IMAGE_GIF,                 /**< Indicates the subsidiary content is gif for example: Content-Type: image/gif */
    EN_HTTP_MSUBTYPE_IMAGE_BMP,                 /**< Indicates the subsidiary content is bmp for example: Content-Type: image/bmp */
    EN_HTTP_MSUBTYPE_IMAGE_PNG,                 /**< Indicates the subsidiary content is png for example: Content-Type: image/png */
    EN_HTTP_MSUBTYPE_AUDIO_AMR,                 /**< Indicates the subsidiary content is audio for example: Content-Type: audio/amr */
    EN_HTTP_MSUBTYPE_VIDEO_MPEG,                /**< Indicates the subsidiary content is mpeg for example: Content-Type: video/mpeg */
    EN_HTTP_MSUBTYPE_VIDEO_3GP,                 /**< Indicates the subsidiary content is 3gp for example: Content-Type: video/3gp */
    EN_HTTP_MSUBTYPE_VIDEO_3G2,                 /**< Indicates the subsidiary content is 3g2 for example: Content-Type: video/3g2 */
    EN_HTTP_MSUBTYPE_VIDEO_3GPP,                /**< Indicates the subsidiary content is 3gpp for example: Content-Type: video/3gpp */
    EN_HTTP_MSUBTYPE_VIDEO_TS,                  /**< Indicates the subsidiary content is ts for example: Content-Type: video/ts */
    EN_HTTP_MSUBTYPE_VIDEO_MP4,                 /**< Indicates the subsidiary content is mp4 for example: Content-Type: video/mp4 */
    EN_HTTP_MSUBTYPE_VIDEO_WEBM,       /**< Indicates the subsidiary content is webm for example: Content-Type: video/webm */
    EN_HTTP_MSUBTYPE_VIDEO_WVM,        /**< Indicates the subsidiary content is wvm for example: Content-Type: video/wvm */
    EN_HTTP_MSUBTYPE_VIDEO_RM,         /**< Indicates the subsidiary content is rm for example: Content-Type: video/rm */
    EN_HTTP_MSUBTYPE_VIDEO_RMVB,       /**< Indicates the subsidiary content is rmvb for example: Content-Type: video/rmvb */
    EN_HTTP_MSUBTYPE_VIDEO_RV,         /**< Indicates the subsidiary content is rv for example: Content-Type: video/rv */
    EN_HTTP_MSUBTYPE_VIDEO_WMV,        /**< Indicates the subsidiary content is wmv for example: Content-Type: video/wmv */
    EN_HTTP_MSUBTYPE_VIDEO_ASF,        /**< Indicates the subsidiary content is asf for example: Content-Type: video/asf */
    EN_HTTP_MSUBTYPE_VIDEO_MPG,        /**< Indicates the subsidiary content is mpg for example: Content-Type: video/mpg */
    EN_HTTP_MSUBTYPE_VIDEO_M4V,        /**< Indicates the subsidiary content is m4v for example: Content-Type: video/m4v */
    EN_HTTP_MSUBTYPE_VIDEO_MKV,        /**< Indicates the the subsidiary content is mkv for example: Content-Type: video/mkve */
    EN_HTTP_MSUBTYPE_VIDEO_AVI,        /**< Indicates the subsidiary content is avi for example: Content-Type: video/avi */
    EN_HTTP_MSUBTYPE_VIDEO_FLV,        /**< Indicates the subsidiary content is flv for example: Content-Type: video/flv */
    EN_HTTP_MSUBTYPE_JSON,             /**< Indicates the subsidiary content is json for example: Content-Type: application/json */
    
    EN_HTTP_MSUBTYPE_EXT               /**< Indicates the subsidiary content is extern type for example: Content-Type: text/xml;charset=UTF-8 */
    
} EN_HTTP_MSUBTYPE;


/** 
 * <b>Description:</b> It provides an entry to use HTTP service. 
 * <br><b>Purpose:</b> The UI accesses HTTP APIs to implement HTTP service.
 */
@interface CaasHttp: NSObject
{
    
}

/**
 * <b>Description:</b> This method is used to initialize the HTTP component. 
 * <br><b>Purpose:</b> The UI invokes it to initialize HTTP component during system initialization before login.
 * @return 0 Indicates it succeed to initialize
 *    <br> not 0 Indicates it failed to initialize
 */
+ (int)initApi;

/**
 * <b>Description:</b> This method is used to alloc a HTTP message object. 
 * <br><b>Purpose:</b> when UI wants to send a HTTP message, the UI invokes it to alloc a HTTP message object.
 * @param uiCookie  Specifies the ID of the HTTP message
 * @return Indicates the HTTP object
 */
-(id)init:(unsigned int)uiCookie;

/**
 * <b>Description:</b> It is used to send a HTTP Message. 
 * <br><b>Purpose:</b> After the UI make a caas Message, UI will can invoke this method to send a HTTP Message.
 * @param caasMsg  Specifies the Caas Message
 * @return 0 Indicates it succeed to send
 *    <br> not 0 Indicates it failed to send
 */
- (int)sendMsg:(CaasMsg *)caasMsg;

/**
 * <b>Description:</b> This method is used to set a string value to a configuration item.
 * <br><b>Purpose:</b> UI can invoke this method to set a string value to a configuration item.
 *
 * @param [in] cfgName Specifies the configure name
 * @param [in] val Specifies the configure value
 *
 * @return 0 Indicates the setting succeeds
 *    <br> not 0 Indicates the setting fails
 */
+ (int)setString: (unsigned int)cfgName val:(NSString *)val;

@end

#endif

/**
 * @file CaasSockCfg.h
 * @brief sock configuration Interface Functions
 */
#ifndef _CAAS_SOCK_CFG_H_
#define _CAAS_SOCK_CFG_H_

#import <Foundation/Foundation.h>

/** It indicates enumeration constants of configuration type */
typedef enum tag_SOCK_CFG_TYPE
{
    USOCK_CFG_TRUST_CA_FILE              = 5,    /**< @brief Indicates that the configuration of the tls trust ca file.*/
    USOCK_CFG_PROXY_MODE                 = 10,   /**< @brief Indicates that the configuration type of message backup proxy mode.*/
    USOCK_CFG_PROXY_DOMAIN               = 11,   /**< @brief Indicates that the configuration type of message backup proxy server domain.*/
    USOCK_CFG_PROXY_NAME                 = 12,   /**< @brief Indicates that the configuration type of message backup proxy authname.*/
    USOCK_CFG_PROXY_PASSWD               = 13,   /**< @brief Indicates that the configuration type of message backup proxy password.*/
    USOCK_CFG_PROXY_SERVER_IP            = 14,   /**< @brief Indicates that the configuration type of message backup proxy server address.*/
    USOCK_CFG_PROXY_SERVER_PORT          = 15,   /**< @brief Indicates that the configuration type of message backup proxy server port.*/
    USOCK_CFG_TLS_MODE                   = 17,   /**< @brief the tls method. */
    USOCK_CFG_TLS_VERIFY_MODE            = 18,   /**< @brief Indicates that the configuration of the tls verify mode.*/
    USOCK_CFG_IPV6_PRIORITY              = 20    /**< @brief Indicates whether ipv6 is the prior ip type. */
} SOCK_CFG_TYPE;

typedef enum tag_UTLS_METHOD_TYPE
{
    EN_USOCK_CFG_UTLS_METHOD_VER1_0      = 0,   /* TLSv1.0 and DTLSv1.0 Client method, default,VER1_0 is unsafe.. */
    EN_USOCK_CFG_UTLS_METHOD_VER1_2      = 2,   /* TLSv1.2 and DTLSv1.2 method,VER1_2 is safe.. */
} EN_USOCK_UTLS_METHOD_TYPE;

typedef enum tag_UTLS_VERIFY_MODE
{
    EN_USOCK_CFG_UTLS_VERY_NONE          = 0,   /* verify none, default and check for cfg */
    EN_USOCK_CFG_UTLS_VERY_REQUIRE       = 1,   /* verify require */
    EN_USOCK_CFG_UTLS_VERY_OPTION        = 2,   /* verify option with no ca */
    EN_USOCK_CFG_UTLS_VERY_OPTION_NO_CA  = 3,   /* verify option with no ca */
} EN_USOCK_UTLS_VERIFY_MODE;


/**
 * <b>Description:</b> CaasSockCfg provides an entry to set and query message backup configuration.
 * <br><b>Purpose:</b> It provides the entry to set and query message backup configuration.
 */
@interface CaasSockCfg : NSObject
{

}


/**
 * <b>Description:</b> This method is used to set a unsigned int value to a configuration item.
 * <br><b>Purpose:</b> UI can invoke this method to set a unsigned int value to a configuration item.
 *
 * @param [in] cfgName Specifies the configure name
 * @param [in] val Specifies the configure value
 *
 * @return 0 Indicates the setting succeeds
 *    <br> not 0 Indicates the setting fails
 */
+ (int)setUint: (unsigned int)cfgName val:(unsigned int)val;

/**
 * <b>Description:</b> This method is used to set a string value to a configuration item.
 * <br><b>Purpose:</b> UI can invoke this method to set a string value to a configuration item.
 *
 * @param [in] cfgName Specifies the configure name
 * @param [in] val Specifies the configure value
 *
 * @return 0 Indicates the setting succeeds
 *    <br> not 0 Indicates the setting fails
 */
+ (int)setString: (unsigned int)cfgName val:(NSString *)val;

/**
 * <b>Description:</b> This method is used to get an unsigned int value of a configuration item.
 * <br><b>Purpose:</b> UI invokes this method to get an unsigned int value of a configuration item.
 *
 * @param [in] cfgName Specifies the configure name
 *
 * @return The value indicates the current configure value
 */
+ (unsigned int)getUint: (unsigned int)cfgName;

/**
 * <b>Description:</b> This method is used to get a string value of a configuration item.
 * <br><b>Purpose:</b> UI invokes this method to get a string value of a configuration item.
 *
 * @param [in] cfgName Specifies the configure name
 *
 * @return The value indicates the current configure value
 */
+ (NSString *)getString: (unsigned int)cfgName;

@end

#endif

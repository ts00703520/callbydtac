/**
 * @file CaasMsg.h
 * @brief Login Caas message Interface Functions
 */
#ifndef _CAAS_MSG_H
#define _CAAS_MSG_H

#import <Foundation/Foundation.h>

 /**< @brief Indicates the Caas message type. */
typedef struct stru_caas_msg
{
    unsigned int unused;
}ST_CASS_MSG;

/**
 * <b>Description:</b> It provides an entry to CAAS Message service.
 * <br><b>Purpose:</b> The UI accesses the APIs to implement CAAS Message service.
 */
@interface CaasMsg: NSObject
{
@private
    
    ST_CASS_MSG *_msgId;
    
}

/** Indicates the caas message ID */
@property(nonatomic,assign) ST_CASS_MSG *msgId;

/**
 * <b>Description:</b> This method provides initialization for login APIs and must be invoked before login.
 * <br><b>Purpose:</b> The UI invokes it to initialize login APIs during system initialization before login.
 * @param uiMsg Specifies the message type
 * @return Indicates the caas message object
 */
- (id)initWithMsg:(unsigned int)uiMsg;

/**
 * <b>Description:</b> This method is used to add an unsigned int value to the CaasMessage.
 * <br><b>Purpose:</b> UI can invoke this method to add an unsigned int value to the CaasMessage.
 *
 * @param [in] uiTag Specifies the configure ID
 * @param [in] uiVal Specifies the configure value
 * @return 0 Indicates the adding succeeds
 *    <br> not 0 Indicates the adding fails
 */
- (int)addUint: (unsigned int)uiTag val:(unsigned int)val;

/**
 * <b>Description:</b> This method is used to add a String value to the CaasMessage.
 * <br><b>Purpose:</b> UI can invoke this method to add a String value to the CaasMessage.
 *
 * @param [in] uiTag Specifies the configure ID
 * @param [in] strVal Specifies the configure value
 * @return 0 Indicates the adding succeeds
 *    <br> not 0 Indicates the adding fails
 */
- (int)addString: (unsigned int)uiTag val:(NSString *)val;

/**
 * <b>Description:</b> This method is used to add a Byte array to the CaasMessage.
 * <br><b>Purpose:</b> UI can invoke this method to add a Byte array to the CaasMessage.
 *
 * @param [in] uiTag Specifies the configure ID
 * @param [in] val Specifies a Byte array
 * @return 0 Indicates the adding succeeds
 *    <br> not 0 Indicates the adding fails
 */
-(int)addByteArray: (unsigned int)uiTag val:(NSData *)val;

/**
 * <b>Description:</b> This method is used to get an unsigned int value of the CaasMessage.
 * <br><b>Purpose:</b> UI invokes this method to get an unsigned int value of the CaasMessage.
 *
 * @param [in] uiTag Specifies the configure ID
 * @param [in] uiDft Specifies the default value
 *
 * @return The value indicates the current configure value
 */
- (unsigned int)getUint: (unsigned int)uiTag defaultval:(unsigned int)defaultval;

/**
 * <b>Description:</b> This method is used to get a string value of the CaasMessage.
 * <br><b>Purpose:</b> UI invokes this method to get a string value of the CaasMessage.
 *
 * @param [in] uiTag Specifies the configure ID
 *
 * @return The value indicates the current configure value
 */
- (NSString *)getString: (unsigned int)uiTag;

/**
 * <b>Description:</b> This method is used to get a Byte array of the CaasMessage.
 * <br><b>Purpose:</b> UI invokes this method to get a Byte array of the CaasMessage.
 *
 * @param [in] uiTag Specifies the configure ID
 *
 * @return The value indicates the current configure value
 */
- (NSData *)getByteArray: (unsigned int)uiTag;

@end

#endif
//
//  HmeAudio.h
//  sa
//
//  Created on 13-07-27.
//  Copyright (c) 2013年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


/** Indicates the phone type */
typedef enum tag_CALL_HME_AUDIO_LEVEL
{
    CALL_HME_LOG_LEVEL_NONE,        /**< ����ӡ��־ */
    CALL_HME_LOG_LEVEL_ERR,         /**< ���� */
    CALL_HME_LOG_LEVEL_INFO,        /**< ��ʾ */
    CALL_LOG_MODE_DEFAULT           /**< Ĭ�ϣ���ʾͬʱ��ӡ�������ʾ��Ϣ*/    
} CALL_HME_AUDIO_LEVEL;

@interface HmeAudio : NSObject

+ (int)setup;

+(int) callHmeAudioLogLevel:(long)dwMode size:(long)dwSizeinKbytes;

@end

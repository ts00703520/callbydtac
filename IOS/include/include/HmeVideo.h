//
//  HmeVideo.h
//  sa
//
//  Created  on 13-07-27.
//  Copyright (c) 2013年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

/** Indicates the phone type */
typedef enum tag_CALL_HME_VIDEO_LEVEL
{
    CALL_HME_V_LOG_NONE  = 0,     /**< ����־ */
    CALL_HME_V_LOG_API   = 1,     /**< �û�������Ϣ����־�����٣����鿪�������ӿڵĴ��� */
    CALL_HME_V_LOG_ERROR = 2,     /**< ��������I/O�����ڲ����� */
    CALL_HME_V_LOG_IO    = 3,     /**< ��������I/O���������Ϣ */
    CALL_HME_V_LOG_ALL   = 4      /**< ������Ϣ����ӡ���е�����Ϣ����־���ܴ�) */   
} CALL_HME_VIDEO_LEVEL;

@interface HmeVideo : NSObject

+ (int)setup;

/**
 * <b>Description:</b> This method is used to query the SDK video engine version number.
 * <br><b>Purpose:</b> The UI invokes it to query the SDK video engine version number after 
 *  call module and hme video module Initialization.
 * @return The value indicates the SDK video engine version number
 */
+ (NSString*)getVersion;

+(int) callHmeVideoLogLevel:(long)dwMode;
@end
    

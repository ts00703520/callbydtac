/**
 * @file CaasImbCfg.h
 * @brief imb configuration Interface Functions
 */
#ifndef _CAAS_IMB_CFG_H_
#define _CAAS_IMB_CFG_H_

#import <Foundation/Foundation.h>

/** It indicates enumeration constants of configuration type */
typedef enum tag_IMB_CFG_TYPE
{
    IMB_CFG_SERVER_ADDR            = 0,  /**< @brief Indicates that the configuration type of message backup server address. */
    IMB_CFG_SERVER_PORT            = 1,  /**< @brief Indicates that the configuration type of message backup server port. */
    IMB_CFG_TRANSFER_TYPE          = 2,  /**< @brief Indicates that the configuration type of transfer type. */
    IMB_CFG_FILE_STORE_PATH        = 3,  /**< @brief Indicates that the configuration type of file store path. */
    IMB_CFG_FILE_THUMB_PATH        = 4,  /**< @brief Indicates that the configuration type of file thumbnail store path. */
    IMB_CFG_AVAIL_STORE_SPACE      = 5,  /**< @brief Indicates that the configuration type of available store space. */
    IMB_CFG_VCARD_STORE_PATH       = 6,  /**< @brief Indicates that the configuration type of vcard store path. */
    IMB_CFG_VCARD_THUMB_PATH       = 7,  /**< @brief Indicates that the configuration type of vcard thumbnail store path. */
} IMB_CFG_TYPE;


/**
 * <b>Description:</b> CaasImbCfg provides an entry to set message backup configuration.
 * <br><b>Purpose:</b> It provides the entry to set message backup configuration.
 */
@interface CaasImbCfg : NSObject
{
}


/**
 * <b>Description:</b> This method is used to set a unsigned int value to a configuration item.
 * <br><b>Purpose:</b> UI can invoke this method to set a unsigned int value to a configuration item.
 *
 * @param [in] cfgName Specifies the configure name
 * @param [in] val Specifies the configure value
 *
 * @return 0 Indicates the setting succeeds
 *    <br> not 0 Indicates the setting fails
 */
+ (int)setUint: (unsigned int)cfgName val:(unsigned int)val;

/**
 * <b>Description:</b> This method is used to set a string value to a configuration item.
 * <br><b>Purpose:</b> UI can invoke this method to set a string value to a configuration item.
 *
 * @param [in] cfgName Specifies the configure name
 * @param [in] val Specifies the configure value
 *
 * @return 0 Indicates the setting succeeds
 *    <br> not 0 Indicates the setting fails
 */
+ (int)setString: (unsigned int)cfgName val:(NSString *)val;

/**
 * <b>Description:</b> This method is used to get an unsigned int value of a configuration item.
 * <br><b>Purpose:</b> UI invokes this method to get an unsigned int value of a configuration item.
 *
 * @param [in] cfgName Specifies the configure name
 *
 * @return The value indicates the current configure value
 */
+ (unsigned int)getUint: (unsigned int)cfgName;

/**
 * <b>Description:</b> This method is used to get a string value of a configuration item.
 * <br><b>Purpose:</b> UI invokes this method to get a string value of a configuration item.
 *
 * @param [in] cfgName Specifies the configure name
 *
 * @return The value indicates the current configure value
 */
+ (NSString *)getString: (unsigned int)cfgName;

@end

#endif

/**
 * @file CaasOmp.h
 * @brief Login OMP Interface Functions
 */
#ifndef _CAAS_OMP_H
#define _CAAS_OMP_H

#import <Foundation/Foundation.h>
#import "CaasMsg.h"

/**< Indicates the default priority of omp component. */
#define    OMP_DFT_PRIORITY           110

/**
 * Indicates the cookie value in the notification NSDictionary.
 */
#define PARAM_OMP_COOKIE         @"PARAM_OMP_COOKIE"

/**
 * Indicates the response message statecode return by the server in the notification NSDictionary.
 */
#define PARAM_OMP_RSP_STACODE        @"PARAM_OMP_RSP_STACODE"

/**
 * Indicates the response message body return by the server in the notification NSDictionary.
 */
#define PARAM_OMP_RSP_BODY        @"PARAM_OMP_RSP_BODY"

/**
 * Indicates the length of response message body return by the server in the notification NSDictionary.
 */
#define PARAM_OMP_RSP_BODY_LEN        @"PARAM_OMP_RSP_BODY_LEN" 

/**
 * Indicates the selfopen business type set by UI in the notification NSDictionary.
 */
#define PARAM_OMP_SELFOPEN_BUSI_TYPE         @"PARAM_OMP_SELFOPEN_BUSI_TYPE"

/**
 * <b>Description:</b> It is used to broadcast a result of OMP selfopen message.
 * <br><b>Purpose:</b> After the UI invokes the sendSelfOpenMsg method, UI will receives the notification, UI can get the information from the server.
 * <p>The notification will have the following extra parameters:
 * <ul>
 * <li><em>{@link #PARAM_OMP_COOKIE}</em></li>- Indicates OMP message cookie, [unsigned int]
 * <li><em>{@link #PARAM_OMP_RSP_STACODE}</em></li>- Indicates OMP message statecode, [unsigned int]
 * <li><em>{@link #PARAM_OMP_RSP_BODY_LEN}</em></li>- Indicates the length of OMP message body, [unsigned int]
 * <li><em>{@link #PARAM_OMP_RSP_BODY}</em></li>- Indicates OMP message body when its length is more than 0, [NSString *]
 * <li><em>{@link #PARAM_OMP_SELFOPEN_BUSI_TYPE}</em></li>- Indicates selfopen business type, [unsigned int]
 * </ul>
 * </p>
 */
#define EVENT_OMP_MESSAGE_RSP        @"EVENT_OMP_MESSAGE_RSP"


/** 
 * <b>Description:</b> It provides an entry to login omp server. 
 * <br><b>Purpose:</b> The UI accesses CaasOmp APIs to implement logining omp.
 */
@interface CaasOmp: NSObject
{
    
}

/** 
* <b>Description:</b> This method provides initialization for login APIs and must be invoked before login. 
* <br><b>Purpose:</b> The UI invokes it to initialize login APIs during system initialization before login.
* @return 0 Indicates it succeed to initialize
*    <br> not 0 Indicates it failed to initialize
*/
+ (int)init;

/** 
* <b>Description:</b> This method is used to release login APIs before logging out of the RCS client.
* <br><b>Purpose:</b> The UI invokes it to release login APIs before logging out of the RCS client.
* @return 0 Indicates it succeed to destroy
*    <br> not 0 Indicates it failed to destroy
*/
+ (int)destroy;


/**
 * <b>Description:</b> This method is used to add the omp component to login. 
 * <br><b>Purpose:</b> The UI invokes it to initialize omp component during system initialization before login.
 * @param [in] uiPriority Indicates the priority of omp component, the default value is OMP_DFT_PRIORITY
 * @return 0 Indicates it succeed to add the omp component
 *    <br> not 0 Indicates it failed to add the omp component
 */
+ (int)addPlugin: (unsigned int)uiPriority;

/**
 * <b>Description:</b> This method is used to remove the omp component to login. 
 * <br><b>Purpose:</b> The UI invokes it to initialize omp component during system initialization before login.
 * @param [in] uiPriority Indicates the priority of omp component, the default value is OMP_DFT_PRIORITY
 * @return 0 Indicates it succeed to remove the omp component
 *    <br> not 0 Indicates it failed to remove the omp component
 */
+ (int)rmvPlugin: (unsigned int)uiPriority;

/**
 * <b>Description:</b> It is used to send a selfopen Message. 
 * <br><b>Purpose:</b> After the UI make a caas Message, UI will can invoke this method to send a selfopen Message.
 * @param uiCookie Specifies the selfopen message id.
 * @param uiBusinessType Specifies the selfopen business type.
 * @return 0 Indicates it succeed to send
 *    <br> not 0 Indicates it failed to send
 */
- (int)sendSelfOpenMsg: (unsigned int)uiCookie uiBusinessType:(unsigned int) uiBusinessType;
@end

#endif


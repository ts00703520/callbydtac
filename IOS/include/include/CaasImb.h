/**
 * @file CaasImb.h
 * @brief imb Interface Functions
 */
#ifndef _CAAS_IMB_H_
#define _CAAS_IMB_H_

#import <Foundation/Foundation.h>
#import "CaasImbCfg.h"

/** It indicates enumeration constants of result code of server response */
typedef enum tag_IMB_RETCODE
{
    IMB_RETCODE_OK, /**< @brief Indicates the response from server is correct. */
    IMB_RETCODE_NO  /**< @brief Indicates the response from server is wrong. */
} IMB_RETCODE;

/** It indicates enumeration constants of sub result code of server response */
typedef enum tag_IMB_RETSUBCODE
{
    IMB_RETSUBCODE_OK, /**< @brief Indicates the response from server is correct. */
    IMB_RETSUBCODE_NO, /**< @brief Indicates the response from server is wrong and the response code is NO. */
    IMB_RETSUBCODE_BAD /**< @brief Indicates the response from server is wrong and the response code is BAD. */
} IMB_RETSUBCODE;

/** It indicates enumeration constants of error code of server response */
typedef enum tag_IMB_ERRCODE
{
    IMB_ERRCODE_CLIENT, /**< @brief Indicates client error. */
    IMB_ERRCODE_TRANS,  /**< @brief Indicates transfer error. */
    IMB_ERRCODE_SERVER  /**< @brief Indicates server error. */
} IMB_ERRCODE;

/** It indicates enumeration constants of sub error code of server response */
typedef enum tag_IMB_ERRSUBCODE
{
    IMB_ERRSUBCODE_NO,      /**< @brief Indicates unknown sub error. */
    IMB_ERRSUBCODE_INNER,   /**< @brief Indicates SDK inner sub error. */
    IMB_ERRSUBCODE_TIMEOUT, /**< @brief Indicates time out sub error. */
    IMB_ERRSUBCODE_TRANS,   /**< @brief Indicates transfer sub error. */
    IMB_ERRSUBCODE_REQTYPE, /**< @brief Indicates request type sub error. */
    IMB_ERRSUBCODE_DNS,     /**< @brief Indicates DNS query sub error. */
    IMB_ERRSUBCODE_BUSY,    /**< @brief Indicates SDK busy sub error. */
    IMB_ERRSUBCODE_RETTAG   /**< @brief Indicates server response tag sub error. */
} IMB_ERRSUBCODE;

/** It indicates the request type */
typedef enum tag_IMB_REQ_TYPE
{
    IMB_REQ_TYPE_LOGIN,          /**< @brief Indicates request type is login. */
    IMB_REQ_TYPE_LIST,             /**< @brief Indicates request type is list. */     
    IMB_REQ_TYPE_SELECT,        /**< @brief Indicates request type is select. */
    IMB_REQ_TYPE_FETCH,          /**< @brief Indicates request type is fetch. */
    IMB_REQ_TYPE_DELETE,        /**< @brief Indicates request type is delete. */
    IMB_REQ_TYPE_STORE,         /**< @brief Indicates request type is store. */
    IMB_REQ_TYPE_CLOSE,         /**< @brief Indicates request type is close. */
    IMB_REQ_TYPE_LOGOUT,      /**< @brief Indicates request type is logout. */
    IMB_REQ_TYPE_MSGID_STORE,      /**< @brief Indicates request type delete message by globalMsgId. */
    IMB_REQ_TYPE_BUTT = 255   /**< @brief Indicates request type is invalid. */
} IMB_REQ_TYPE;

/** 
 * Indicates the base folder path of user queried.
 */
#define PARAM_IMB_LIST_PATH @"CAASIMB_PARAM_LIST_PATH"

/** 
 * Indicates the full folder path of user queried.
 */
#define PARAM_IMB_LIST_NAME @"CAASIMB_PARAM_LIST_NAME"

/** 
 * Indicates the history message number of folder.
 */
#define PARAM_IMB_EXISTS_NUM @"CAASIMB_PARAM_EXISTS_NUM"

/**
 * Indicates the uid next to the max uid of message in the folder.
 */
#define PARAM_IMB_EXISTS_UID_NEXT @"CAASIMB_PARAM_EXISTS_UID_NEXT"

/** 
 * Indicates the result code of server response.
 * @see tag_IMB_RETCODE
 */
#define PARAM_IMB_RESULTCODE @"CAASIMB_PARAM_RESULTCODE"

/** 
 * Indicates the sub result code of server response.
 * @see tag_IMB_RETSUBCODE
 */
#define PARAM_IMB_RESULTSUBCODE @"CAASIMB_PARAM_RESULTSUBCODE"

/** 
 * Indicates the error code of server response.
 * @see tag_IMB_ERRCODE
 */
#define PARAM_IMB_ERRORCODE @"CAASIMB_PARAM_ERRORCODE"

/** 
 * Indicates the sub error code of server response.
 * @see tag_IMB_ERRSUBCODE
 */
#define PARAM_IMB_ERRORSUBCODE @"CAASIMB_PARAM_ERRORSUBCODE"

/** 
 * Indicates the request type.
 * @see tag_IMB_REQ_TYPE
 */
#define PARAM_IMB_REQTYPE @"CAASIMB_PARAM_REQTYPE"

/**
 * <b>Description:</b> It is used to broadcast to UI that some other command should be send to server.
 * <br><b>Purpose:</b> The UI may receive this broadcast when UI has called some interfaces before, 
 * it will tell the UI that some other interface should be called until completion.
 */ 
#define EVENT_IMB_CONTINUE @"CAASIMB_EVENT_CONTINUE"

/**
 * <b>Description:</b> It is used to broadcast that the messages loaded from CMS have been wirtten to DB ok.
 * <br><b>Purpose:</b> The UI will receive this broadcast when messages have been written to DB ok, 
 *  UI can receive this broadcast and remind the user.
 */ 
#define EVENT_IMB_WRITE_DB_OK @"CAASIMB_EVENT_WRITE_DB_OK"

/**
 * <b>Description:</b> It is used to broadcast the message folders path of current user on message backup server.
 * <br><b>Purpose:</b> Before UI fetch history message from message backup server, it should query the path of history message save.
 * After UI call interface to query, it will receive this broadcast.
 * <p>The broadcast will have the following extra parameters:
 * <ul>
 * <li><em>{@link #PARAM_IMB_LIST_PATH}</em></li> - Identifies the base path of user queried [string]
 * <li><em>{@link #PARAM_IMB_LIST_NAME}</em></li> - Identifies the full path of history message save [string]
 * </ul>
 * </p>
 */
#define EVENT_IMB_LIST @"CAASIMB_EVENT_LIST"

/**
 * <b>Description:</b> It is used to broadcast the number of history message in the folder which UI queried.
 * <br><b>Purpose:</b> Before UI fetch history message in a folder, it should obtain the detailed information of this folder.
 * After UI call interface to obtain the information, it will receive this broadcast.
 * <p>The broadcast will have the following extra parameters:
 * <ul>
 * <li><em>{@link #PARAM_IMB_EXISTS_NUM}</em></li> - Identifies the number of history message [int]
 * </ul>
 * </p>
 */
#define EVENT_IMB_EXISTS @"CAASIMB_EVENT_EXISTS"

/**
 * <b>Description:</b> It is used to broadcast the uid next to the max uid of history message in the folder which UI queried.
 * <br><b>Purpose:</b> Before UI fetch history message in a folder, it should obtain the detailed information of this folder.
 * After UI call interface to obtain the information, it will receive this broadcast.
 * <p>The broadcast will have the following extra parameters:
 * <ul>
 * <li><em>{@link #PARAM_IMB_EXISTS_UID_NEXT}</em></li> - Identifies the uid next to the max uid of message in the folder [int]
 * </ul>
 * </p>
 */
#define EVENT_IMB_UID_NEXT @"CAASIMB_EVENT_UID_NEXT"

/**
 * <b>Description:</b> It is used to broadcast the result of server response.
 * <br><b>Purpose:</b> After UI call interface to send request to server, server may return one or more response 
 * and the last response indicates the response before is right or not. UI can receive this broadcast and remind the user.
 * <p>The broadcast will have the following extra parameters:
 * <ul>
 * <li><em>{@link #PARAM_IMB_RESULTCODE}</em></li> - Identifies the result code [int]
 * <li><em>{@link #PARAM_IMB_RESULTSUBCODE}</em></li> - Identifies the sub result code [int]
 * </ul>
 * </p>
 */
#define EVENT_IMB_RESULT @"CAASIMB_EVENT_RESULT"

/**
 * <b>Description:</b> It is used to broadcast the error of client and server and network.
 * <br><b>Purpose:</b> The UI will receive this broadcast when some error happen, UI can receive this broadcast and remind the user.
 * <p>The broadcast will have the following extra parameters:
 * <ul>
 * <li><em>{@link #PARAM_IMB_ERRORCODE}</em></li> - Identifies the error code [int]
 * <li><em>{@link #PARAM_IMB_ERRORSUBCODE}</em></li> - Identifies the sub error code [int]
 * </ul>
 * </p>
 */
#define EVENT_IMB_ERROR @"CAASIMB_EVENT_ERROR"

/**
 * <b>Description:</b> CaasImb provides an entry to fetch history message from message backup service.
 * <br><b>Purpose:</b> It provides the entry to fetch history message from message backup service.
 */
@interface CaasImb : NSObject
{
}

/**
 * <b>Description:</b> This method provides initialization for message backup component.
 * <br><b>Purpose:</b> The UI invokes this method to initialize the message backup function during system initialization.
 */
+ (void) init;

/**
 * <b>Description:</b> This method is used to release message backup component.
 * <br><b>Purpose:</b> TThe UI invokes this method to destroy message backup component before a subscriber exits the application.
 */
+ (void) release;

/**
 * <b>Description:</b> This method is used to login the message backup server. 
 * <br><b>Purpose:</b> The UI should invokes this method to login the message backup server before fetch history message. 
 * @param userName  Specifies user name.
 * @param password  Specifies password.
 */
+ (void) login:(NSString *)userName password:(NSString *)password;

/**
 * <b>Description:</b> This method is used to query the folders path where the history message been stored. 
 * <br><b>Purpose:</b> The history message been stored on message backup server by folder. Each folder indicates a conversation. 
 * The UI invokes this method to query conversation list on server, which means query the folders path on server. 
 * @param path  Specifies base path prepare to query.
 */
+ (void) getFloderList:(NSString *)path;

/**
 * <b>Description:</b> This method is used to query detailed information of one folder on the message backup server. 
 * <br><b>Purpose:</b> The UI should select the folder and get detailed information of this folder before UI fetch 
 * the history message of one conversation.
 * @param path  Specifies base path prepare to query.
 */
+ (void) getFloderDetail:(NSString *)path;

/**
 * <b>Description:</b> This method is used to fetch history message of the selected folder on server. 
 * <br><b>Purpose:</b> The UI invokes this method to fetch history message on server. UI must select one folder before invokes this method.
 * @param startNum  Specifies start number of message sequence prepare to fetch.
 * @param endNum  Specifies end number of message sequence prepare to fetch.
 */
+ (void) recoverMsg:(int)startNum endNum:(int)endNum;

/**
 * <b>Description:</b> This method is used to download the content of one history file message. 
 * <br><b>Purpose:</b> The content of history file message do not download first time. The UI need invokes this method to get.
 * @param keyId  Specifies message id.
 */
+ (void) getFileContent:(long)keyId;

/**
 * <b>Description:</b> This method is used to delete history message from message backup server. 
 * <br><b>Purpose:</b> If one message is history message, the UI should invokes this method to delete the history message 
 * from message backup server when UI delete it on local database.
 * @param startNum  Specifies start number of message sequence prepare to delete.
 * @param endNum  Specifies end number of message sequence prepare to delete.
 */
+ (void) deleteMsg:(int)startNum endNum:(int)endNum;

/**
 * <b>Description:</b> This method is used to delete history message from message backup server.
 * <br><b>Purpose:</b> If one message is history message, the UI should invokes this method to delete the history message
 * from message backup server when UI delete it on local database.
 * @param globalMsgId  Specifies the globalMsgId of message  to delete.
 */
+ (void) deleteMsgByMsgId:(NSString *)globalMsgId;

/**
 * <b>Description:</b> This method is used to delete the all message in a conversation on message backup server. 
 * <br><b>Purpose:</b> The UI should invokes this method to delete the folder of conversation when UI delete the conversation on local database.
 * @param path  Specifies the path of folder prepare to delete.
 */
+ (void) deleteFloder:(NSString *)path;

/**
 * <b>Description:</b> This method is used to close the select state of one folder. 
 * <br><b>Purpose:</b> When UI finish to fetch the history message, UI should invokes this method to close the select state of this folder.
 */
+ (void) close;

/**
 * <b>Description:</b> This method is used to logout from message backup server. 
 * <br><b>Purpose:</b> When UI finish to fetch the history message, UI should invokes this method to logout from the message backup server.
 */
+ (void) logout;

/**
 * <b>Description:</b> This method is used to cancel the method UI invokes before. 
 * <br><b>Purpose:</b> When UI do not want to continue the process, UI should invokes this method and the SDK state return to idle.
 */
+ (void) cancel;

@end

#endif

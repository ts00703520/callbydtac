/**
 * @file MeetingApi.h
 * @brief Meeting Interface Functions
 */

#import <Foundation/Foundation.h>
#import "CallApi.h"

@class MeetingParams;
@class ExtParams;
/** used to provide an entry for joining conferences. 
 The UI accesses specific conference joining-related interfaces by invoking interfaces of this class.
 */
@interface MeetingApi : CallApi
{

}

/**
 * used to quickly join an audio conference 
 * <p>
 * After the UI receives a conference invitation, it obtains the conference number, ID, and password and invokes this interface to quickly join the audio conference
 * </p>
 * @param number Indicates the number of the conference to be joined
 * @param confId Indicates the ID of the conference to be joined
 * @param confPwd Indicates the password of the conference to be joined.
 * @return A conference session object (call session object)
 */
+(CallSession*) quickJoinAudioConf:(NSString*) number confId:(NSString*)confId confPwd:(NSString*)confPwd;

+(CallSession*) quickJoinDesktopSharingConf:(NSString*) number confId:(NSString*)confId confPwd:(NSString*)confPwd;

/**
 * used to quickly join a video conference
 * <p>
 * After the UI receives a conference invitation, it obtains the conference number, ID, and password and invokes this interface to quickly join the video conference.
 * </p>
 * @param number Indicates the number of the conference to be joined
 * @param confId Indicates the ID of the conference to be joined
 * @param confPwd Indicates the password of the conference to be joined.
 * @param localView Indicates the video view on the local software client
 * @param remoteView ndicates the video view on the peer software client
 * @return A conference session object (call session object)
 */
+(CallSession*) quickJoinVideoConf:(NSString*) number confId:(NSString*)confId confPwd:(NSString*)confPwd localView:(EAGLExView*) localView remoteView:(EAGLExView*) remoteView;

/**
 * used to quickly join an audio conference 
 * <p>
 * After the UI receives a conference invitation, it obtains the conference number, ID, and enableData and invokes this interface to quickly join the audio conference
 * </p>
 * @param number Indicates the number of the conference to be joined
 * @param confId Indicates the ID of the conference to be joined
 * @param confPwd Indicates the enableData of the conference to be joined.
 * @return A conference session object (call session object)
 */
+(CallSession*) callAudioConf:(NSString*) number confId:(NSString*)confId enableData:(NSString*)enableData;

/**
 * used to join an audio conference with password
 * <p>
 * After the UI receives a conference invitation, it obtains the conference number, ID, and enabledata and
 invokes this interface to quickly join the audio conference
 * </p>
 * @param number Indicates the number of the conference to be joined
 * @param confId Indicates the ID of the conference to be joined
 * @param enableData Indicates the enabledata of the conference to be joined.
 * @param password Indicates the password of the conference to be joined.
 * @return A conference session object (call session object)
 */
+ (CallSession*) callAudioConf:(NSString*) number confId:(NSString*)confId enableData:(NSString*)enableData password:(NSString *)password;

/**
 * used to quickly join a video conference
 * <p>
 * After the UI receives a conference invitation, it obtains the conference number, ID, and enableData and invokes this interface to quickly join the video conference.
 * </p>
 * @param number Indicates the number of the conference to be joined
 * @param confId Indicates the ID of the conference to be joined
 * @param confPwd Indicates the enableData of the conference to be joined.
 * @param localView Indicates the video view on the local software client
 * @param remoteView ndicates the video view on the peer software client
 * @return A conference session object (call session object)
 */
+(CallSession*) callVideoConf:(NSString*) number confId:(NSString*)confId enableData:(NSString*)enableData localView:(EAGLExView*) localView remoteView:(EAGLExView*) remoteView;

/**
 * used to join a video conference with password
 * <p>
 * After the UI receives a conference invitation, it obtains the conference number, ID, and enabledata and
 invokes this interface to quickly join the video conference.
 * </p>
 * @param number Indicates the number of the conference to be joined
 * @param confId Indicates the ID of the conference to be joined
 * @param enabledata Indicates the enabledata of the conference to be joined.
 * @param password Indicates the password of the conference to be joined.
 * @param localView Indicates the video view on the local software client
 * @param remoteView ndicates the video view on the peer software client
 * @return A conference session object (call session object)
 */
+(CallSession*) callVideoConf:(NSString*) number
                                   confId:(NSString*)confId
                               enableData:(NSString*)enableData
                                 password:(NSString*)password
                                localView:(EAGLExView*) localView
                               remoteView:(EAGLExView*) remoteView;

/**
 * used to quickly join a video conference
 * <p>
 * After the UI receives a conference invitation, it obtains the conference number, ID, and enabledata and
 invokes this interface to quickly join the video conference.
 * </p>
 * @param number Indicates the number of the conference to be joined
 * @param confId Indicates the ID of the conference to be joined
 * @param confPwd Indicates the enabledata of the conference to be joined.
 * @param localView Indicates the video view on the local software client
 * @param remoteView Indicates the video view on the peer software client
 * @param extParams Indicates extended parameters of the call.
 * @return A conference session object (call session object)
 */
+(CallSession*) quickJoinMultiStreamVideoConf:(NSString*) number
                                  confId:(NSString*)confId
                                 confPwd:(NSString*)confPwd
                               localView:(EAGLExView*) localView
                              remoteView:(EAGLExView*) remoteView
                               extParams:(ExtParams*)extParams;

/**
 * used to set the ID and password of a conference to be joined
 * <p> The UI invokes this interface to set the ID and password of a conference to be joined. The UI can directly invoke quickJoinaudioConf or quickJoinVideoConf to join a conference. It can also invoke this interface to set the ID and password of a conference to be joined and then invokes initiateAudioCall or initiateVideoCall to join the conference
 </p>
 * @param confId Indicates the ID of the conference to be joined
 * @param confPwd Indicates the password of the conference to be joined.
 */
+(void)setConferenceInfo:(NSString*)confId confPwd:(NSString*)confPwd;


+(CallSession*) callAudioInstantConf:(NSString*)confFactoryId
                       meetingParams:(MeetingParams*)meetingParams;

+(CallSession*) callVideoInstantConf:(NSString*)confFactoryId
                       meetingParams:(MeetingParams*)meetingParams
                           localView:(EAGLExView*)localView
                          remoteView:(EAGLExView*)remoteView;

@end

/** Indicates the phone type */
typedef enum tag_CALL_EXT_PARA
{
    CALL_EXT_PARAM_LOCAL_AUDIO_IP,         /**< Indicates local audio ip. */
    CALL_EXT_PARAM_LOCAL_AUDIO_PORT,       /**< Indicates local audio port. */
    CALL_EXT_PARAM_LOCAL_VIDEO_IP,         /**< Indicates local video ip. */
    CALL_EXT_PARAM_LOCAL_VIDEO_PORT,       /**< Indicates local video port. */
    CALL_EXT_PARAM_AUDIO_URI_FORMAT,       /**< Indicates Indicates audio uri format. */
    CALL_EXT_PARAM_CALL_TERM_REASON,       /**< Indicates call termination reason. */
    CALL_EXT_PARAM_CALL_TYPE,              /**< Indicates call type. */
    CALL_EXT_PARAM_CALL_SRV_TYPE,          /**< Indicates call termination type. */
    CALL_EXT_PARAM_CONF_MULTISTREAM = 9,    /**< Indicates conference type. */
    CALL_EXT_PARAM_CONF_DESKTOPSHARING
}CALL_EXT_PARA;

/** Indicates the MRP window type */
typedef enum tag_CALL_MULTISTRM_WINDOW_TYPE
{
    CALL_MULTISTRM_BROADCAST,        /**< Indicates the type of the video selected: broadcast */
    CALL_MULTISTRM_ATTENDEE,         /**< Indicates the type of the video selected: attendee. */
}CALL_MULTISTRM_WINDOW_TYPE;

/**
 * <b>Description:</b> Attendee is used to describe an attendee of meeting.
 * <br><b>Purpose:</b> The UI invokes interfaces of this class to obtain properties of an attendee.
 */
@interface MeetingAttendee : NSObject

/**
 * <b>Description:</b> This property is used to obtain the name of the attendee .
 */
@property (retain, nonatomic) NSString *name;

/**
 * <b>Description:</b> This property is used to obtain the number of the attendee .
 */
@property (retain, nonatomic) NSString *number;

/**
 * <b>Description:</b> This property is used to obtain the left number of the attendee .
 */
@property (retain, nonatomic) NSString *leftNumber;

/**
 * <b>Description:</b> This property is used to obtain the right number of the attendee .
 */
@property (retain, nonatomic) NSString *rightNumber;

/**
 * <b>Description:</b> This property is used to obtain the email of the attendee .
 */
@property (retain, nonatomic) NSString *email;

/**
 * <b>Description:</b> This property is used to obtain the sms number of the attendee .
 */
@property (retain, nonatomic) NSString *smsNumber;

/**
 * <b>Description:</b> This property is used to obtain the accountId of the attendee .
 */
@property (retain, nonatomic) NSString *accountId;

/**
 * <b>Description:</b> This property is used to obtain the pinCode of the attendee .
 */
@property (retain, nonatomic) NSString *pinCode;

/**
 * <b>Description:</b> This property is used to obtain the role of the attendee .
 */
@property (assign, nonatomic) int role;

@end

/** Indicates the image type */
typedef enum tag_MEETING_IMAGE_TYPE
{
    MEETING_IMAGE_TYPE_SINGLE,           /**< Indicates the image type is single. */
    MEETING_IMAGE_TYPE_TWO,              /**< Indicates the image type is two. */
    MEETING_IMAGE_TYPE_THREE,            /**< Indicates the image type is three. */
    MEETING_IMAGE_TYPE_FOUR,             /**< Indicates the image type is four. */
    MEETING_IMAGE_TYPE_SIX,              /**< Indicates the image type is six. */
    MEETING_IMAGE_TYPE_EIGHT,            /**< Indicates the image type is eight. */
    MEETING_IMAGE_TYPE_NINE,             /**< Indicates the image type is nine. */
    MEETING_IMAGE_TYPE_THIRTEENR,        /**< Indicates the image type is thirteenr. */
    MEETING_IMAGE_TYPE_THIRTEENM,        /**< Indicates the image type is thirteenm. */
    MEETING_IMAGE_TYPE_SIXTEEN,          /**< Indicates the image type is sixteen. */
}MEETING_IMAGE_TYPE;

/** Indicates the switch mode */
typedef enum tag_MEETING_SWITCH_MODE
{
    MEETING_SWITCH_MODE_CHANGEOVERVOICE, /**< Indicates the switch mode is change over voice. */
    MEETING_SWITCH_MODE_FIXATION,        /**< Indicates the switch mode is fixation. */
}MEETING_SWITCH_MODE;

/**
 * <b>Description:</b> MeetingParams indicates the class of a meeting.
 * <br><b>Purpose:</b> When the SDK sends the {@link MeetingBaseApi.EVENT_MEETING_SCHEDULE_MEETING_RSP} broadcast to the UI, the broadcast contains the instances of
 * this class as parameters. After obtaining parameters from the broadcast, the UI invokes the methods of this class to obtain information about meeting,
 * and updates the view based on the information.
 */

@interface MeetingParams : NSObject

/**
 * <b>Description:</b> This property is used to obtain the start time of a conference.
 */
@property (retain, nonatomic) NSString *startTime;

/**
 * <b>Description:</b> This property is used to obtain the subject of a conference.
 */
@property (retain, nonatomic) NSString *subject;

/**
 * <b>Description:</b> This property is used to obtain the meeting id of a conference.
 */
@property (retain, nonatomic) NSString *meetingId;

/**
 * <b>Description:</b> This property is used to obtain the language of a conference.
 */
@property (retain, nonatomic) NSString *language;

/**
 * <b>Description:</b> This property is used to obtain the scheduleName of a conference.
 */
@property (retain, nonatomic) NSString *schedulerName;

/**
 * <b>Description:</b> This property is used to get userType of the meeting.
 */
@property (retain, nonatomic) NSString *userType;

/**
 * <b>Description:</b> This property is used to obtain the time zone of a conference.
 */
@property (assign, nonatomic) int timeZone;

/**
 * <b>Description:</b> This property is used to obtain the size of a conference.
 */
@property (assign, nonatomic) int size;

/**
 * <b>Description:</b> This property is used to obtain the media type of a conference.
 */
@property (assign, nonatomic) int mediaType;

/**
 * <b>Description:</b> This property is used to obtain the encrypt mode of a conference.
 */
@property (assign, nonatomic) int encryptMode;

/**
 * <b>Description:</b> This property is used to obtain the state of a conference.
 */
@property (assign, nonatomic) int state;

/**
 * <b>Description:</b> This property is used to obtain the length of a conference.
 */
@property (assign, nonatomic) long length;

/**
 * <b>Description:</b> This property is used to check whether the conference is a cycle conference.
 */
@property (assign, nonatomic) BOOL isCycleType;

/**
 * <b>Description:</b> This property is used to check whether participants are automatically invited to party.
 */
@property (assign, nonatomic) BOOL isAutoInvite;

/**
 * <b>Description:</b> This property is used to check whether the conference allow recording.
 */
@property (assign, nonatomic) BOOL isAllowRecord;

/**
 * <b>Description:</b> This property is used to check whether the conference auto prolong.
 */
@property (assign, nonatomic) BOOL isAutoProlong;

/**
 * <b>Description:</b> This property is used to check whether the conference auto record.
 */
@property (assign, nonatomic) BOOL isAutorecord;

/**
 * <b>Description:</b> This property is used to check whether the conference wait chairman.
 */
@property (assign, nonatomic) BOOL isWaitChairman;

/**
 * <b>Description:</b> This property is used to obtain the attendee array of a conference.
 */
@property (retain, nonatomic) NSMutableArray *attendeeArray;

/**
 * <b>Description:</b> This property is used to obtain the image type of a conference.
 */
@property (assign, nonatomic) int imageType;

/**
 * <b>Description:</b> This property is used to obtain the switch mode of a conference.
 */
@property (assign, nonatomic) int switchMode;

@end

/**
 * <b>Description:</b> extParams is used to add some extended parameters.
 * <br><b>Purpose:</b> The UI invokes interfaces of this class to add some extra
 parameters.
 */
@interface ExtParams : NSObject

/**
 * <b>Description:</b> <b>Description:</b> This property is used to whether the conference is
 multistream or not. 1 for multistream, 0 for single stream.
 */
@property (assign, nonatomic) int isMultiStrm;

@end

#ifndef __CAAS_SOCK_API_H__
#define __CAAS_SOCK_API_H__

//#include "caas_sys.h"


#ifdef __cplusplus
extern "C" {
#endif

typedef void          CAAS_VOID; /**< Indicates type of void. */
typedef char          CAAS_CHAR; /**< Indicates type of char. */
typedef int           CAAS_INT;  /**< Indicates type of int. */
typedef int           CAAS_BOOL; /**< Indicates type of int. */
typedef unsigned char CAAS_UCHAR;/**< Indicates type of unsigned char. */
typedef unsigned int  CAAS_UINT; /**< Indicates type of unsigned int. */
typedef unsigned long CAAS_ULONG;/**< Indicates type of unsigned long. */
typedef unsigned short CAAS_USHORT;/**< Indicates type of unsigned short. */

#define CAAS_OK     0 /**< Indicates ok */
#define CAAS_FAILED 1 /**< Indicates failed */
#define CAAS_NULL   0 /**< Indicates null */
#define CAAS_TRUE   1 /**< Indicates true */
#define CAAS_FALSE  0 /**< Indicates false */

#ifdef CAAS_SYS_EXPORTS
#define CAAS_SYS_API __declspec(dllexport) /**< @brief export sys api */
#else
#define CAAS_SYS_API                       /**< @brief import sys api */
#endif

/** Indicates the stg socket transfer normol data(eg. control signaling). use TLS */
#define CAAS_SOCK_FLAG_NULL          0x00000000               /* flag */
/** Indicates the stg socket transfer media data(eg. media, file). use DTLS, high efficiency*/
#define CAAS_SOCK_FLAG_MEDIA         0x00000001               /* media trans flag */

/** Indicates the stg socket handle*/
typedef CAAS_VOID* CAAS_SOCK;

/** Indicates the stg socket type, use in ST_CAAS_SOCK_CFG ucType*/
typedef enum enum_CAAS_SOCK_TYPE
{
    EN_CAAS_SOCK_UDP       = 0,  
    EN_CAAS_SOCK_TCP_CLI   = 1,    
    EN_CAAS_SOCK_BUTT
} EN_CAAS_SOCK_TYPE;

/** Indicates type define of callback function used to notify user recv data from udp. */
typedef CAAS_UINT (*PFN_CAAS_SOCK_ONRECVUDP)(CAAS_SOCK hSock,  CAAS_UCHAR *pucData, CAAS_UINT uiLen, CAAS_UCHAR *pucRmtAddr, CAAS_UINT uiRmtPort);
/** Indicates type define of callback function used to notify user recv data from tcp. */
typedef CAAS_UINT (*PFN_CAAS_SOCK_ONRECVTCP)(CAAS_SOCK hSock,  CAAS_UCHAR *pucData, CAAS_UINT uiLen);
/** Indicates type define of callback function used to notify user send continue. */
typedef CAAS_INT (*PFN_CAAS_SOCK_ONSENDTCP)(CAAS_SOCK hSock,  CAAS_INT *pbDone);
/** Indicates type define of callback function used to notify user the stg socket connect. */
typedef CAAS_INT (*PFN_CAAS_SOCK_ONCONNECT)(CAAS_SOCK hSock);
/** Indicates type define of callback function used to notify user the stg socket disconnect. */
typedef CAAS_INT (*PFN_CAAS_SOCK_ONDISCTCP)(CAAS_SOCK hSock);

/** Indicates type define of callback functions. */
typedef struct stru_CAAS_SOCK_CB
{
    PFN_CAAS_SOCK_ONRECVUDP    pfnOnRecvUdp;
    PFN_CAAS_SOCK_ONRECVTCP    pfnOnRecvTcp;
    PFN_CAAS_SOCK_ONSENDTCP    pfnOnSendTcp;
    PFN_CAAS_SOCK_ONCONNECT    pfnOnConnect;
    PFN_CAAS_SOCK_ONDISCTCP    pfnOnDiscTcp;
} ST_CAAS_SOCK_CB;

/** Indicates type define of socket config. use to create socket. UI should set the struct memery to 0 before use it*/
typedef struct stru_CAAS_SOCK_CFG
{
    CAAS_UCHAR ucType;          /* see EN_CAAS_SOCK_TYPE */
    CAAS_UCHAR ucReserved[3];   /* reserved */

    CAAS_UCHAR *pucLclAddr;     /* stg socket local ipv4 addr */
    CAAS_UINT  uiLclPort;       /* stg socket local port */
    CAAS_UINT  uiFalg;          /* mask, see CAAS_SOCK_FLAG_NULL */
    CAAS_UINT  uiSendMaxCount;  /* send buf list count 0 means caas socket default send max count : 1024 */
    CAAS_UINT  uiRecvMaxLen;    /* tcp recv buffer max length, 0 means use caas socket default buffer size 20M */
    CAAS_UINT  uiRecvBlockLen;  /* tcp recv buffer memroy alloc once length, 0 means use caas socket default readbuffer size 16k */

    ST_CAAS_SOCK_CB stCb;       /* stg socket callback functions list */
} ST_CAAS_SOCK_CFG;

typedef struct stru_CAAS_STG_CFG
{
    CAAS_UCHAR *pucStgLoginName; /* config stg server login name */
    CAAS_UCHAR *pucStgPassWord;  /* config stg server password */
    CAAS_UCHAR *pucStgServerIp;  /* config stg server Ip */
    CAAS_UINT  uiStgServerPort;  /* config stg server port */
} ST_CAAS_STG_CFG;

/**
 * <b>Description:</b> This method is used to config stg param.
 * <br><b>Purpose:</b> The UI invokes it to config stg param, before invokes it UI should fill a ST_CAAS_STG_CFG.
 * @param [in] ST_CAAS_STG_CFG Specifies the stg config
 */
CAAS_SYS_API CAAS_VOID CAAS_SockCfgStg(ST_CAAS_STG_CFG *pstStgCfg);
    
/**
 * <b>Description:</b> This method is used to create a stg socket.
 * <br><b>Purpose:</b> The UI invokes it to create a stg socket, before invokes it UI should fill a ST_CAAS_SOCK_CFG.
 * @param [in] ST_CAAS_SOCK_CFG Specifies the socket config
 *
 * @return CAAS_SOCK Indicates the created socket's handle.
 */
CAAS_SYS_API CAAS_SOCK CAAS_SockCreate(ST_CAAS_SOCK_CFG *pstCfg);

/**
 * <b>Description:</b> This method is used to Connect to remote ip, port.
 * <br><b>Purpose:</b> The UI invokes it to Connect to ip, port after socket create.
 * @param [in] hSock Specifies the socket handle
 * @param [in] pucRmtAddr Specifies the remote ip
 * @param [in] uiRmtPort Specifies the remote port
 *
 * @return 0 Indicates it succeed
 *    <br> not 0 Indicates it failed
 */
CAAS_SYS_API CAAS_INT  CAAS_SockConnect(CAAS_SOCK hSock, CAAS_UCHAR *pucRmtAddr, CAAS_UINT uiRmtPort);

/**
 * <b>Description:</b> This method is used to send data in the socket.
 * <br><b>Purpose:</b> The UI invokes it to send data in the socket after UI recv callback pfnOnConnect.
 * @param [in] hSock Specifies the socket handle
 * @param [in] pucData Specifies the send data
 * @param [in] uiLen Specifies the send data length
 *
 * @return 0 Indicates it succeed
 *    <br> not 0 Indicates it failed
 */
CAAS_SYS_API CAAS_INT  CAAS_SockSend(CAAS_SOCK hSock, CAAS_UCHAR *pucData, CAAS_UINT uiLen);

/**
 * <b>Description:</b> This method is used to send data to the remote ip, port.
 * <br><b>Purpose:</b> The UI invokes it to send data to the remote ip, port after socket create.
 * @param [in] hSock Specifies the socket handle
 * @param [in] pucRmtAddr Specifies the remote ip
 * @param [in] uiRmtPort Specifies the remote port
 * @param [in] pucData Specifies the send data
 * @param [in] uiLen Specifies the send data length
 *
 * @return 0 Indicates it succeed
 *    <br> not 0 Indicates it failed
 */
CAAS_SYS_API CAAS_INT  CAAS_SockSendTo(CAAS_SOCK hSock, CAAS_UCHAR *pucRmtAddr, CAAS_UINT uiRmtPort, CAAS_UCHAR *pucData, CAAS_UINT uiLen);

/**
 * <b>Description:</b> This method is used to close socket.
 * <br><b>Purpose:</b> The UI invokes it to close socket if UI do not want to use it anymore.
 * @param [in] hSock Specifies the socket handle
 */
CAAS_SYS_API CAAS_VOID CAAS_SockClose(CAAS_SOCK *phSock);

/**
 * <b>Description:</b> This method is used to get local ip, port.
 * <br><b>Purpose:</b> The UI invokes it to get local ip, port after UI recv callback pfnOnConnect.
 * @param [in] hSock Specifies the socket handle
 * @param [out] pucLclAddr Specifies the local ip
 * @param [out] uiLclPort Specifies the local port
 *
 * @return 0 Indicates it succeed
 *    <br> not 0 Indicates it failed
 */
CAAS_SYS_API CAAS_INT  CAAS_SockGetLclAddr(CAAS_SOCK hSock, CAAS_UCHAR *pucLclAddr, CAAS_UINT *puiLclPort);
//#endif
#ifdef __cplusplus
}
#endif

#endif /* __CAAS_SOCK_API_H__ */

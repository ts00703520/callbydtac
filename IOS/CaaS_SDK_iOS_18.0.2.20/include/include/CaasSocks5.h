/**
 * @file CaasSocks5.h
 */
#ifndef _CAAS_SOCKS5_H
#define _CAAS_SOCKS5_H

#import <Foundation/Foundation.h>

/** 
 * <b>Description:</b> It provides an entry to use SOCKS5 service. 
 * <br><b>Purpose:</b>  The UI accesses SOCKS5 APIs to implement SOCKS5 service.
 */
@interface CaasSocks5: NSObject
{
    
}

/**
 * <b>Description:</b> This method is used to initialize the SOCKS5 component. 
 * <br><b>Purpose:</b> The UI invokes it to initialize SOCKS5 component during system initialization before login.
 * @return 0 Indicates it succeed to initialize
 *    <br> not 0 Indicates it failed to initialize
 */
+ (int)init;

/** 
* <b>Description:</b> This method is used to destroy the socks5 component.
* <br><b>Purpose:</b> The UI invokes it to destroy socks5 component when logging out of the client.
* @return 0 Indicates it succeed to destroy
*    <br> not 0 Indicates it failed to destroy
*/
+ (int)destroy;


/**
 * <b>Description:</b> This method is used to test whether the socks5 server is available.
 * <br><b>Purpose:</b> The UI invokes it to test whether the socks5 server is available before login.
 * @param [in] AuthName Specifies not brief Proxy name
 * @param [in] Passwd Specifies not brief Proxy password
 * @param [in] ProxyIp Specifies not brief Proxy ip
 * @param [in] ProxyPort Specifies not brief Proxy port
 * @return 0 Indicates the socks5 server is available.
 *    <br> not 0 Indicates it failed to negotiate with the socks5 server.
 */
+ (int)proxytest: (int) ProxyPort AuthName:(char*) AuthName Passwd:(char*) Passwd ProxyIp:(char*) ProxyIp;

@end

#endif



 

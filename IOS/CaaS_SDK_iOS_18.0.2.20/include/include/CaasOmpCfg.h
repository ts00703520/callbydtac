/**
 * @file CaasOmpCfg.h
 */
#ifndef _CAAS_OMP_CFG_H
#define _CAAS_OMP_CFG_H

#import <Foundation/Foundation.h>

typedef enum tag_OMP_CFG_TYPE 
{
    EN_OMP_CFG_SERVER_IP               =   0,  /**< Indicates the server ip [NSString]. */
    EN_OMP_CFG_SERVER_PORT             =   1,  /**< Indicates the server port [int]. */
    EN_OMP_CFG_APP_KEY                 =   2,  /**< Indicates the APP key [NSString]. */
    EN_OMP_CFG_USRNAME                 =   3,  /**< Indicates the user name [NSString]. */
    EN_OMP_CFG_PASSWORD                =   4,  /**< Indicates the password  [NSString]. */
    EN_OMP_CFG_METHOD_TYPE             =   5,  /**< Indicates the selfopen message type  [unsigned int]. */
    EN_OMP_CFG_URL                     =   6,  /**< Indicates the selfopen url  [NSString]. */
    EN_OMP_CFG_BODY                    =   7,  /**< Indicates the selfopen message body  [NSString]. */
    EN_OMP_CFG_SELFOPEN_TIMEOUT        =   8,  /**< Indicates the wainting selfopen message response time  [int]. */ 
    EN_OMP_CFG_SERVER_SPARE_IP0        =   9,  /**< Indicates the server spare ip0 [NSString]. */
    EN_OMP_CFG_SERVER_SPARE_PORT0      =   10,  /**< Indicates the server spare port0 [int]. */
    EN_OMP_CFG_SERVER_SPARE_IP1        =   11,  /**< Indicates the server spare ip1 [NSString]. */
    EN_OMP_CFG_SERVER_SPARE_PORT1      =   12,  /**< Indicates the server spare port1 [int]. */
    EN_OMP_CFG_SERVER_SPARE_IP2        =   13,  /**< Indicates the server spare ip2 [NSString]. */
    EN_OMP_CFG_SERVER_SPARE_PORT2      =   14,  /**< Indicates the server spare port2 [int]. */
    EN_OMP_CFG_MAX                             /**< Indicates the butt. */
} EN_OMP_CFG_TYPE;

typedef enum tag_OMP_MSG_TYPE 
{
    EN_OMP_CFG_METHOD_GET               =   1,  /**< Indicates the server port [int]. */
    EN_OMP_CFG_METHOD_POST              =   3,  /**< Indicates the APP key [NSString]. */
    EN_OMP_CFG_METHOD_PUT               =   4,  /**< Indicates the user name [NSString]. */
    EN_OMP_CFG_METHOD_DELETE            =   5,  /**< Indicates the password  [NSString]. */
} EN_OMP_MSG_TYPE;

typedef enum tag_OMP_SELFOPEN_BUSI_TYPE 
{
    EN_OMP_CFG_GET_PICCODE              =   0,  /**< Indicates the picture code business. */
    EN_OMP_CFG_GET_SMSCODE              =   1,  /**< Indicates the sms code business. */
    EN_OMP_CFG_REG_APPUSER              =   2,  /**< Indicates the user register business. */
    EN_OMP_CFG_CANCEL_APPUSER           =   3,  /**< Indicates the cancel user business. */
    EN_OMP_CFG_RESET_PASSWORD           =   4,  /**< Indicates the reset password business. */
    EN_OMP_CFG_MODIFY_PASSWORD          =   5,  /**< Indicates the modify password business. */
    EN_OMP_CFG_QUERY_PASSWORD           =   6,  /**<Indicates the query password business.*/
    EN_OMP_CFG_REG_MULTIVIDEO           =   7,  /**<Indicates the register mulit-video business.*/
    EN_OMP_CFG_CANCEL_MULTIVIDEO        =   8,  /**<Indicates the cancel mulit-video business.*/
    EN_OMP_CFG_BUSINESS_MAX                     /**< Indicates the butt. */
} EN_OMP_SELFOPEN_BUSI_TYPE;

/** 
 * <b>Description:</b> It provides an entry to login omp server. 
 * <br><b>Purpose:</b> The UI accesses CaasOmp APIs to implement logining omp.
 */
@interface CaasOmpCfg: NSObject
{
    
}

/**
 * <b>Description:</b> This method is used to set a unsigned int value to a configuration item.
 * <br><b>Purpose:</b> UI can invoke this method to set a unsigned int value to a configuration item.
 *
 * @param [in] cfgName Specifies the configure name
 * @param [in] val Specifies the configure value
 *
 * @return 0 Indicates the setting succeeds
 *    <br> not 0 Indicates the setting fails
 */
+ (int)setUint: (unsigned int)cfgName val:(unsigned int)val;

/**
 * <b>Description:</b> This method is used to set a string value to a configuration item.
 * <br><b>Purpose:</b> UI can invoke this method to set a string value to a configuration item.
 *
 * @param [in] cfgName Specifies the configure name
 * @param [in] val Specifies the configure value
 *
 * @return 0 Indicates the setting succeeds
 *    <br> not 0 Indicates the setting fails
 */
+ (int)setString: (unsigned int)cfgName val:(NSString *)val;

/**
 * <b>Description:</b> This method is used to get an unsigned int value of a configuration item.
 * <br><b>Purpose:</b> UI invokes this method to get an unsigned int value of a configuration item.
 *
 * @param [in] cfgName Specifies the configure name
 *
 * @return The value indicates the current configure value
 */
+ (unsigned int)getUint: (unsigned int)cfgName;

/**
 * <b>Description:</b> This method is used to get a string value of a configuration item.
 * <br><b>Purpose:</b> UI invokes this method to get a string value of a configuration item.
 *
 * @param [in] cfgName Specifies the configure name
 *
 * @return The value indicates the current configure value
 */
+ (NSString *)getString: (unsigned int)cfgName;

@end

#endif